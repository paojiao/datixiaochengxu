/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-08 09:42:10
 * @LastEditTime: 2019-09-30 15:08:23
 * @LastEditors: Please set LastEditors
 */
const apis = require("./apiC.js")
// console.log(apis.default)
//全局缓存
const store = (data,time) =>{
      // console.log(data,time)
      var newtime = new Date();
      var endDate = new Date(time.getTime() + 1000 * 60); //存入时间
      
      // var endDate = (new Date(time)).getTime();

      wx.setStorageSync('oldtime', endDate)

      // var nuu = (new Date(n)).getTime();

      
      // console.log(endDate)
      // if(newtime >nuu || endDate ==nuu){
      //             console.log('已过期')
      // }
      // localStorage.setItem('yy',data)
      wx.setStorageSync('goods', JSON.stringify(data))
}

//百度经纬度转腾讯经纬度
const bMapTransQQMap = (lng, lat) => {
      // console.log(lng,lat)
          let x_pi = 3.14159265358979324 * 3000.0 / 180.0;
          let x = lng - 0.0065;
          let y = lat - 0.006;
          let z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
          let theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
          let lngs = z * Math.cos(theta);
          let lats = z * Math.sin(theta);
      // console.log("++++++++++++++++++++++++++++++++++++++++++")
      // console.log(lngs,lats)
          return {
              lng: lngs,
              lat: lats        
          }   
    }

     //微信支付配置函数
const requestPayment = (timeStamp,nonceStr,packageId,signType,paySign,isfrom,machineName) => {
      
      var timeStampy = JSON.stringify(timeStamp)
      var thyg = this;
      console.log(timeStampy,nonceStr,packageId,signType,paySign)
      wx.requestPayment(
            {
            'timeStamp':timeStampy,
            'nonceStr': nonceStr,
            'package':packageId,
            'signType': signType,
            'paySign': paySign ,
            'success':function(res){
                  if(isfrom == 'pay'){
                        // console.log(thyg)
                        thyg.default.jump.commuunalPage('恭喜您在'+machineName+'开机成功啦')
                  }else{
                        wx.showToast({
                              title: '充值成功',
                              icon: 'success',
                              duration: 2000
                        })
                  }
            },
            'fail':function(res){
                  wx.showToast({
                        title: '支付失败',
                        icon: 'none',
                        duration: 2000
                  })
            },
            'complete':function(res){
                  console.log(res)
            }
            })
} 

/**
	 * 格式化时间
	 * @param date
	 * @param fmt
	 * @returns {*}
	 */
	const format= (date) =>{
		var yy = date.getFullYear();      //年
	var mm = date.getMonth() + 1;     //月
		var dd = date.getDate();          //日
		var hh = date.getHours();         //时
		var ii = date.getMinutes();       //分
		var ss = date.getSeconds();       //秒
		var clock = yy + "-";
		if (mm < 10) clock += "0";
		clock += mm + "-";
		if (dd < 10) clock += "0";
		clock += dd + " ";
		if (hh < 10) clock += "0";
        clock += hh + ":";
		if (ii < 10) clock += '0';
		clock += ii + ":";
		if (ss < 10) clock += '0';
		clock += ss;
		return clock;
	}

	const getDate = (date) =>{
		var d = new Date(date);
            var d1 = format(d);
		return d1;
      }
      
      //拿取code函数
const codeFun = (codecball) =>{
            wx.login({
                  success: function (res) {
                        codecball(res.code)
                  }
            })
      }

      //公用模态显示框
const  showModal = (content,calby) =>{
      wx.showModal({
            title:'提示',
            content: content,
            confirmText: "确认",
            cancelText: "取消",
            success: function (res) {
                           if (res.confirm) {
                              //    console.log('确认')
                                 calby('confirm')
                           }else{
                              //    console.log('取消')
                                 calby('cancel')
                           }
                       }
      })
}  
     //公用确认显示框
const  Modal = (content) =>{
      wx.showModal({
            title:'提示',
            content: content,
            showCancel: false,
            success: function (res) {
            }
      })
}  


      // 公共加载显示框
const loading = (content)=>{
      wx.showLoading({
            title: content
      })
}



// 页面跳转函数
const jump={
      // 跳转接口调用失败的函数
      jumpFail(){
            wx.showToast({
                  title: '系统错误，请联系管理',
                  icon: 'none',
                  duration: 2000
            })
      },
      // 发布页面
      releasePage(){
            wx.navigateTo({
                  url:'/pages/release/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 优惠券页面
      couponPage(machId,orderId,machineName){
            if(this.loginPage()) return
            wx.navigateTo({
                  url:'/pages/coupon/main?machId='+machId+"&orderId="+orderId+"&machineName="+machineName,
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 充值记录
      payLogPage(){
            if(this.loginPage()) return
            wx.navigateTo({
                  url:'/pages/payLog/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 洗车记录页面
      washRecordPage(){
            if(this.loginPage()) return
            wx.navigateTo({
                  url:'/pages/washRecord/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 邀请好友
      invitationPage(){
            if(this.loginPage()) return
            wx.navigateTo({
                  url:'/pages/invitation/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 邀请纪律
      invitationLogPage(){
            wx.navigateTo({
                  url:'/pages/invitationLog/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 代洗列表
      subsWashPage(){
            wx.showToast({
                  title: '代洗功能还未上线\n敬请期待',
                  icon: 'none',
                  duration: 2000
            })
      },
      // 清楚缓存
      clearCache(){
            wx.clearStorage({
                  success(){
                      wx.showToast({
                              title:'清除已清除',
                              icon:'success',
                              duration:2000,
                              success(){
                                    wx.navigateTo({
                                    url: '/pages/my/main'
                              })
                          }
                      })
                  },
                  fail(){
                      wx.showToast({
                          title:'系统错误',
                          icon:'none',
                          duration:2000
                      })
                  }
            })
      },
      // 帮助
      helpPage(){
            wx.navigateTo({
                  url:'/pages/teach/main',
                  fail(){
                        jump.jumpFail()
                  }
            })
      },
      // 发现页面
      findPage(params){
            let str = ''
            for(let key in params){
                  str+=key+'='+params[key]+'&'
            }
            str = str.slice(0,str.length-1)
            wx.switchTab({
                  url: "/pages/find/main?"+str
            })
      },
      // 提示页面
      commuunalPage(conetent){
            wx.navigateTo({
                  url:"/pages/communal/main?msg="+conetent
            })
            
      },
      // 登录页面
      loginPage(){
            // console.log('login')
            // wx.navigateTo({
            //       url: '/pages/login/main'
            // })
            if(!wx.getStorageSync('userInfo')){
                  wx.showModal({
                        title:'此功能需登录',
                        content:'点击确认跳转登录~',
                        success(res){
                              if(res.confirm){
                                    wx.navigateTo({
                                          url:'/pages/my/main'
                                    })
                              }else{
                              
                              }
                        }
                  })
                  return true
            }
      }
}

 //点赞功能
 const ismoty = (uid,thy,numys) => { //numys=1列表点赞 为2时详情点赞
      // console.log(uid)
      var washy;
       if(numys == 1){
            washy = thy.washnumber;
            for(var i=0;i<washy.length;i++){
                  if(washy[i].uid == uid){
                          if(washy[i].isGood == 0){
                              thy.$set(thy.washnumber,'isGood',washy[i].isGood++);
                              thy.$set(thy.washnumber,'goodS',washy[i].goodS++);
                              publicFun(0)
                          }else{
                              thy.$set(thy.washnumber,'isGood',washy[i].isGood--);
                              thy.$set(thy.washnumber,'goodS',washy[i].goodS--);
                              publicFun(1)
                          }
                  }
              }
       }else{
            washy = thy.newdata;
            if(washy.isGood == 0){
                  thy.$set(thy.newdata,'isGood',++washy.isGood);
                  thy.$set(thy.newdata,'goodS',++washy.goodS);
                  thy.$set(thy.newdata,'goodList',thy.newdata.goodList.concat({headImg:thy.datas.userInfo.headImg,nickName:thy.datas.userInfo.nickName}))
                  console.log(thy.newdata.goodList)
                  publicFun(0)
              }else{
                  thy.$set(thy.newdata,'isGood',--washy.isGood);
                  thy.$set(thy.newdata,'goodS',--washy.goodS);
                  for(var k =0;k<thy.newdata.goodList.length;k++){
                        if(thy.newdata.goodList[k].nickName == thy.datas.userInfo.nickName){
                              thy.newdata.goodList.splice(k,1)
                              thy.$set(thy.newdata,'goodList',thy.newdata.goodList)
                        }
                  }
                  
                  publicFun(1)
              }
       }
     

        function publicFun(nums){
            let publicFuny = (datay) => {
            //     console.log(datay)
            }
            if(nums == 0){
                  apis.default.cardOperation('post',{userId:wx.getStorageSync('userInfo').userId,infoId:uid,operationType:3},publicFuny)
            }else{
                  apis.default.delCardOperation('post',{userId:wx.getStorageSync('userInfo').userId,infoId:uid,operationType:3},publicFuny)
            }
        }
}

const filderName = names =>{
      var newNmae;
      if(names == undefined){
      return names;
      }
      if(names.length>28){
      newNmae = names.substring(0,28);
      newNmae = newNmae+"··"
      return newNmae
      }else{
      return names;
      }
}


export default {
      store:store,
      bMapTransQQMap:bMapTransQQMap,
      requestPayment:requestPayment,
      codeFun:codeFun,
      showModal:showModal,
      Modal:Modal,
      loading,
      jump,
      ismoty:ismoty,
      getDate:getDate,
      filderName:filderName
}