/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-05 09:48:48
 * @LastEditTime: 2019-09-05 09:49:58
 * @LastEditors: Please set LastEditors
 */
import App from './index'
import Vue from 'vue'

const app = new Vue(App)
app.$mount()